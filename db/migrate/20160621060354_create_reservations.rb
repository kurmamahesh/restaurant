class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :customer
      t.string :phone_number
      t.integer :number_of_people
      t.datetime :reservation_datetime

      t.timestamps null: false
    end
  end
end
