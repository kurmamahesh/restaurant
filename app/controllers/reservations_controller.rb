class ReservationsController < ApplicationController

def new
    @reservation = Reservation.new
  end

  def create
    @reservation = Reservation.new(reservations_priv)
    @reservation.save
    redirect_to url_for(:controller => :reservations, :action => :index)
  end
  
  def show
    @reservation = Reservation.find(params[:id])
  end
  
  def index
    @reservation = Reservation.all
  end


  private
  def reservations_priv
    params.require(:reservation).permit(:customer, :phone_number, :number_of_people, :reservation_datetime)
  end

end
